let arr = ['hello', 'world', 23, '23', null, undefined, {}, Symbol(), function(){}, null];

function filterBy(array, dataType) {
    
    if (dataType === "null") {
        return array.filter(item => item !== null);
    } else if (dataType === "object") {
        let nonObjectArray = [];

        array.forEach(function(item) {
            if (item === null || typeof item !== "object") {
                nonObjectArray.push(item);
            }
        });

        return nonObjectArray;
    }

    return array.filter(item => typeof item !== dataType);
}

console.log(filterBy(arr, "object"));
console.table(filterBy(arr, "object"));



/* 1. Опишіть своїми словами як працює метод forEach.

Метод forEach приймає в себе callBackFunction, яку буде виконувати для кожного елементу масиву.
*/


/* 2. Як очистити масив?

Можна прописати, що довжина масиву (length) = 0, і тоді в масиві нічого не буде. Також можна застосовувати метод splice(),
 який також дозволяє видаляти елементи масиву, але вже за їх індексом.
*/


/* 3. Як можна перевірити, що та чи інша змінна є масивом?

Можна використати метод Array.isArray - цей метод повертає true, якщо змінна є масивом, і false якщо ні.
*/

